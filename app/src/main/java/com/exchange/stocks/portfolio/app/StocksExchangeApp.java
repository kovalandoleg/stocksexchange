package com.exchange.stocks.portfolio.app;

import android.app.Application;

import io.paperdb.Paper;

public class StocksExchangeApp extends Application {
    @Override
    public void onCreate() {

        super.onCreate();
        Paper.init(this);
    }
}

package com.exchange.stocks.portfolio;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SelectPairActivity extends AppCompatActivity implements DownloadCallback, AdapterView.OnItemSelectedListener {
    // Keep a reference to the NetworkFragment, which owns the AsyncTask object
    // that is used to execute network ops.
    private NetworkFragment mNetworkFragment;

    // Boolean telling us whether a download is in progress, so we don't trigger overlapping
    // downloads with consecutive button clicks.
    private boolean mDownloading = false;

    private Spinner spinner;
    private String selectedExchange;
    private ListView listView;
    ArrayAdapter<String> adapter;
    private Result downloadResult;

    //Temporary Global Class Container
    Globals GLOBALS = Globals.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_pair);
        listView = findViewById(R.id.pairList);
        spinner = findViewById(R.id.spinnerExchange);
        spinner.setOnItemSelectedListener(this);
        selectedExchange = String.valueOf(spinner.getSelectedItem());
        mNetworkFragment = NetworkFragment.getInstance(getSupportFragmentManager(), "https://stocks.exchange/api2/ticker");
        this.startDownload();
    }

    //Parsing json to tiker obj. should be implement somewhere else:)
    public String parseToTicker(JSONObject jO, int index, ArrayList tikerArr) throws JSONException {
        String market_name = jO.getString("market_name").replace('_', '/');
        Ticker ticker = new Ticker(
                jO.getDouble("min_order_amount"),
                jO.getDouble("ask"),
                jO.getDouble("bid"),
                jO.getDouble("last"),
                jO.getDouble("lastDayAgo"),
                jO.getDouble("vol"),
                market_name, index, jO.getInt("updated_time"));
        tikerArr.add(ticker);
        return market_name;
    }

    public void updateListView(){
        JSONArray jsonArray;
        GLOBALS.setAllTickers(new ArrayList<Ticker>());
        String[] pairs = null;
        if (downloadResult.mResultValue != null){
        try {
            jsonArray = new JSONArray(downloadResult.mResultValue);
            int lenght = jsonArray.length();
            pairs = new String[lenght];
            for (int i = 0; i<lenght; i++){
                pairs[i] = parseToTicker(jsonArray.getJSONObject(i), i, GLOBALS.getAllTickers());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }} else {
            pairs = new String[1];
            pairs[0] = downloadResult.mException.getMessage();
        }

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, pairs);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(adapter);

        //Set true Already selected Tikers
        if (!GLOBALS.getSelectedTickers().isEmpty()){
            for (Ticker tick :
                    GLOBALS.getSelectedTickers()) {
                listView.setItemChecked(tick.getJsonArrayIndex(), true);
            }
        }
        // ***
    }

    public void onClickSelExchNext(View view) {
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        GLOBALS.setSelectedTickers(new ArrayList<Ticker>());
//        int[] che = new int[checked.size()];
        for (int i = 0; i < checked.size(); i++) {
            // Item position in adapter
            int position = checked.keyAt(i);
            // Add if it is checked i.e.) == TRUE!
            if (checked.valueAt(i))
//                selectedItems.add(adapter.getItem(position));
                GLOBALS.getSelectedTickers().add(GLOBALS.getAllTickers().get(position));
        }

        Intent resultIntent = new Intent();
        if (!GLOBALS.getSelectedTickers().isEmpty()) {
            resultIntent.putExtra("selected", GLOBALS.getSelectedTickers());
            setResult(RESULT_OK, resultIntent);
        } else setResult(RESULT_CANCELED);
        finish();
    }

    private void startDownload() {
        if (!mDownloading && mNetworkFragment != null) {
            // Execute the async download.
            mNetworkFragment.startDownload(this);
            mDownloading = true;
        }
    }

    //// DownloadCallback Implementation
    @Override
    public void updateFromDownload(Result result) {
        if (result == null)
            return;
        downloadResult = result;
        updateListView();
    }

    @Override
    public NetworkInfo getActiveNetworkInfo() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo;
    }

    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        switch(progressCode) {
            // You can add UI behavior for progress updates here.
            case Progress.ERROR:
                break;
            case Progress.CONNECT_SUCCESS:
                break;
            case Progress.GET_INPUT_STREAM_SUCCESS:
                break;
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:
                break;
            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                break;
        }
    }

    @Override
    public void finishDownloading() {
        mDownloading = false;
        if (mNetworkFragment != null) {
            mNetworkFragment.cancelDownload();
        }
    }

    //  Spinner implement
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String newSel = (String) adapterView.getItemAtPosition(i);
        //Temporary becaouse no others Exchanges
        if (mNetworkFragment == null) {
            mNetworkFragment = NetworkFragment.getInstance(getSupportFragmentManager(), "https://stocks.exchange/api2/ticker"); ////make Interface with constants!!!
        }
        if (!selectedExchange.equals(newSel)){

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
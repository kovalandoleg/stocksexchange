package com.exchange.stocks.portfolio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.exchange.stocks.portfolio.dummy.DummyContent;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity implements TikerFragment.OnListFragmentInteractionListener {

    //Temporary Global Class Container
    Globals GLOBALS = Globals.getInstance();

    //Fragment RecycleView vars
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    //////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //region Update SSLSocket protocol to use TLS in android <5.0
        try {
            ProviderInstaller.installIfNeeded(getApplicationContext());
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
        //endregion

        setContentView(R.layout.activity_main);

        // ***Fragment RecycleView*** //
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        ///////////////////////////////////////////////////////////////////

        GLOBALS.setSelectedTickers(Paper.book().read("selected", new ArrayList<Ticker>()));

        if (GLOBALS.getSelectedTickers().isEmpty() || GLOBALS.getSelectedTickers() == null){
            Intent intent = new Intent(this, SelectPairActivity.class);
            startActivityForResult(intent, 1);
        } else {
            mAdapter = new MyTikerRecyclerViewAdapter(GLOBALS.getSelectedTickers(), this);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                //Do Something after SelectPair activity return result
                mAdapter = new MyTikerRecyclerViewAdapter(GLOBALS.getSelectedTickers(), this);
                mRecyclerView.setAdapter(mAdapter);

                //try to write data to flash
                Paper.book().write("selected", GLOBALS.getSelectedTickers());
            }
        }
    }

    //Floating Action Add button click
    public void onClickFABaddTicker(View view) {
        Intent intent = new Intent(this, SelectPairActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onListFragmentInteraction(Ticker item) {

    }
}


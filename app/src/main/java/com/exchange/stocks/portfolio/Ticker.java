package com.exchange.stocks.portfolio;

import java.io.Serializable;

/**
 * Created by nook on 13.02.18.
 */

public class Ticker implements Serializable {

    private double minOrder;
    private double maxBuy;
    private double minSell;
    private double lastPrice;
    private double lastPriceAgo;
    private double change24h;
    private double volume24;
    private String marketName;
    private int jsonArrayIndex;
    private int updatedTime;

    public Ticker(double minOrder, double maxBuy, double minSell, double lastPrice, double lastPriceAgo, double volume24, String marketName, int jsonArrayIndex, int updatedTime) {
        this.minOrder = minOrder;
        this.maxBuy = maxBuy;
        this.minSell = minSell;
        this.lastPrice = lastPrice;
        this.lastPriceAgo = lastPriceAgo;
        this.change24h = (lastPrice - lastPriceAgo)*100/lastPriceAgo;
        this.volume24 = volume24;
        this.marketName = marketName;
        this.jsonArrayIndex = jsonArrayIndex;
        this.updatedTime = updatedTime;
    }

    public double getMaxBuy(){
      return maxBuy;
    }

    public double getMinSell() {
        return minSell;
    }

    public double getLastPrice() {
        return lastPrice;
    }

    public double getChange24h() {
        return change24h;
    }

    public double getVolume24() {
        return volume24;
    }

    public String getMarketName() {
        return marketName;
    }

    public int getJsonArrayIndex() {
        return jsonArrayIndex;
    }

    public int getUpdatedTime() {
        return updatedTime;
    }
}

package com.exchange.stocks.portfolio;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.exchange.stocks.portfolio.TikerFragment.OnListFragmentInteractionListener;

import java.util.List;

public class MyTikerRecyclerViewAdapter extends RecyclerView.Adapter<MyTikerRecyclerViewAdapter.ViewHolder> {

    private final List<Ticker> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyTikerRecyclerViewAdapter(List<Ticker> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_tiker, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // TODO: Move Values to resourece strings
        holder.mItem = mValues.get(position);
        holder.mNameView.setText(mValues.get(position).getMarketName());
        holder.mVolView.setText("" + mValues.get(position).getVolume24());
        holder.mLastPriceView.setText("" + mValues.get(position).getLastPrice());
        holder.mMaxBuyView.setText("" + mValues.get(position).getMaxBuy());
        holder.mMinSellView.setText("" + mValues.get(position).getMinSell());
        holder.mChange24hView.setText("" + mValues.get(position).getChange24h());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final TextView mVolView;
        public final TextView mLastPriceView;
        public final TextView mMaxBuyView;
        public final TextView mMinSellView;
        public final TextView mChange24hView;
        public Ticker mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.market_name);
            mVolView = (TextView) view.findViewById(R.id.vol);
            mLastPriceView = (TextView) view.findViewById(R.id.lastPrice);
            mMaxBuyView = (TextView) view.findViewById(R.id.maxBuy);
            mMinSellView = (TextView) view.findViewById(R.id.minSell);
            mChange24hView = (TextView) view.findViewById(R.id.change24h);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mVolView.getText() + "'";
        }
    }
}
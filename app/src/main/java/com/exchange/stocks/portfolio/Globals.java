package com.exchange.stocks.portfolio;

import java.util.ArrayList;

/**
 * Created by nook on 10.04.18.
 */

public class Globals{
    private static Globals instance;

    // Global variable
    private ArrayList<Ticker> allTickers;
    private ArrayList<Ticker> selectedTickers;

    // Restrict the constructor from being instantiated
    private Globals(){}

    public synchronized void setAllTickers(ArrayList<Ticker> all){
        this.allTickers=all;
    }
    public synchronized ArrayList<Ticker> getAllTickers(){
        return this.allTickers;
    }

    public synchronized void setSelectedTickers(ArrayList<Ticker> sel){
        this.selectedTickers=sel;
    }
    public synchronized ArrayList<Ticker> getSelectedTickers(){
        return this.selectedTickers;
    }

    public static synchronized Globals getInstance(){
        if(instance==null){
            instance=new Globals();
        }
        return instance;
    }
}